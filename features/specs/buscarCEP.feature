#language: pt

Funcionalidade: Buscar CEP correio
        Como usuário do sistema correio 
        Eu quero poder pesquisar um cep
        Para obter as informações necessárias

    Esquema do Cenário: Efetuar busca por CEP
        Dado que esteja na página principal do Correio
        Quando eu informar cep <cep>
        Então sistema apresenta dados do CEP <nome><bairro><uf><cep_ret>

Exemplos: 
|cep               |nome                                     |bairro      |uf                       |cep_ret       |
|"88047618"   | "Rua Recanto dos Girassóis"   |"Carianos"|"Florianópolis/SC"|"88047-618"|
          
        
